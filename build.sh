#!/usr/bin/env bash

echo "Starting..."
echo "----------"
echo "# Exclude error-pages from Proxy"
echo "ProxyPass /error-pages/ !"
echo "# Set Error Documents"

## 400 pages
seq_400="$(seq 400 1 417) $(seq 421 1 424) 426 428 429 431 451"
for i in $seq_400; do
  cp "./template.html" "./${i}.html"
  sed -e "s/%ERRORCODE%/${i}/g" \
      -e "s/%ERRORSIMPLE%/4xx/g" \
      -e "s/%ERROREXPLAIN%/The requested content might not be available or you are not allowed to see it./" \
      -i "./${i}.html"
  echo "ErrorDocument ${i} /error-pages/${i}.html"
done

## 500 pages
seq_500="$(seq 500 1 508) 510 511"
for i in $seq_500; do
  cp "./template.html" "./${i}.html"
  sed -e "s/%ERRORCODE%/${i}/g" \
      -e "s/%ERRORSIMPLE%/5xx/g" \
      -e "s/%ERROREXPLAIN%/The Service is currently under maintenance./" \
      -i "./${i}.html"
  echo "ErrorDocument ${i} /error-pages/${i}.html"
done

echo "----------"
echo "Done!"
